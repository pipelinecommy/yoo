# Generated by Django 2.2.6 on 2019-10-04 08:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('workshops', '0002_workshop_location'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='workshop',
            name='location',
        ),
    ]
