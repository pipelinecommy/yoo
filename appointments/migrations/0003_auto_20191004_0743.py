# Generated by Django 2.2.6 on 2019-10-04 07:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('appointments', '0002_auto_20191004_0739'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='appointment',
            name='time',
        ),
        migrations.AlterField(
            model_name='appointment',
            name='kedai',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='workshops.Workshop'),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='kereta',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cars.Car'),
        ),
    ]
