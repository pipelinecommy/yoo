from rest_framework import serializers

from django.utils.timezone import now

from .models import (
   Appointment,
)
class AppointmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Appointment
        fields = '__all__'