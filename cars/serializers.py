from rest_framework import serializers

from django.utils.timezone import now

from .models import (
   Car,

)



class CarSerializer(serializers.ModelSerializer):

    class Meta:
        model = Car
        fields = '__all__'